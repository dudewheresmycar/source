<!DOCTYPE html>
<html lang="en">
    <head>
	   <script type="text/javascript" src="jquery-3.2.1.js"></script>
	   <script language="javascript" type="text/javascript">
	   $(document).ready(function(){
		   console.log($('#PIR1').text())
		   $('body').css('background-image', 'url("dust_scratches.png")');
		   $('#Header').css('font-size', '35px')
		   $('#Header').css('color', 'navy')
		   $('#Header').css('border-style', 'solid')
		   $('#Header').css('border-radius', '8px')
		   $('#Header').css('border-width', '2px')
		   $('#Header').css('margin-right', '1250px')
		   $('#Introduction').css('font-size', '18px')
		   $('#gtitle').css('font-size', '20px')
		   $('#rtitle').css('font-size', '20px')
		   $('#wrapper').css('margin', '0')
		   $('#greensquare').css('float', 'left')
		   $('#gtitle').css('float', 'left')
		   $('#redsquare').css('float', 'left')
		   $('#rtitle').css('float', 'left')
		   $('#legend').css('font-size', '30px')
		   $('#greensquare').css('width', '25')
		   $('#greensquare').css('height', '25')
		   $('#greensquare').css('border', '2px solid black')
		   $('#greensquare').css('background', 'green')
		   $('#redsquare').css('width', '25')
		   $('#redsquare').css('height', '25')
		   $('#redsquare').css('border', '2px solid black')
		   $('#redsquare').css('background', 'red')
		   $('#PIR1_title').css('font-size', '20px')
		   $('#PIR1').css('width', '320')
		   $('#PIR1').css('height', '170')
		   $('#PIR1').css('border', '3px solid black')
		   if ($('#PIR1').text() == 0){
			   $('#PIR1').css('background', 'green')
			   $('#PIR1').css('color', 'green')
		   }else{
			   $('#PIR1').css('background', 'red')
			   $('#PIR1').css('color', 'red')
		   }
		   console.log($('#PIR2').text())
		   $('#PIR2_title').css('font-size', '20px')
		   $('#PIR2').css('width', '320')
		   $('#PIR2').css('height', '170')
		   $('#PIR2').css('border', '3px solid black')
		   if ($('#PIR2').text() == 0){
			   $('#PIR2').css('background', 'green')
			   $('#PIR2').css('color', 'green')
		   }else{
			   $('#PIR2').css('background', 'red')
			   $('#PIR2').css('color', 'red')
		   }
		   console.log($('#PIR3').text())
		   $('#PIR3_title').css('font-size', '20px')
		   $('#PIR3').css('width', '320')
		   $('#PIR3').css('height', '170')
		   $('#PIR3').css('border', '3px solid black')
		   if ($('#PIR3').text() == 0){
			   $('#PIR3').css('background', 'green')
			   $('#PIR3').css('color', 'green')
		   }else{
			   $('#PIR3').css('background', 'red')
			   $('#PIR3').css('color', 'red')
		   }
		   console.log($('#PIR4').text())
		   $('#PIR4_title').css('font-size', '20px')
		   $('#PIR4').css('width', '320')
		   $('#PIR4').css('height', '170')
		   $('#PIR4').css('border', '3px solid black')
		   if ($('#PIR4').text() == 0){
			   $('#PIR4').css('background', 'green')
			   $('#PIR4').css('color', 'green')
		   }else{
			   $('#PIR4').css('background', 'red')
			   $('#PIR4').css('color', 'red')
		   }
	   });
	   </script>
	   <link rel="stylesheet" href="styles.css" type="text/css" />
           <meta charset="UTF-8">
	   <meta http-equiv="refresh" content="3">
           <title>Parking Structure Monitor</title>
           <meta name="description" contents="Just a test website for learning html, css and php">
           <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
	   <div id="Header"> Parking Structure Monitoring System </div>
    <?php
	 #<meta http-equiv="refresh" content="1" >
	    $servername = "localhost";
	    $username = "nbergam";
	    $password = "pubg";
	    $dbname = "Parking_database";
	
	    // Create connection
	    $conn = new mysqli($servername, $username, $password, $dbname);
	    // Check connection
	    if ($conn->connect_error) {
	         die("Connection failed: " . $conn->connect_error);
	    } 
	
	    $sql0 = "SELECT LicensePlate FROM parkingLog";
	    $sql1 = "SELECT PIR1 FROM parkingLog";
	    $sql2 = "SELECT PIR2 FROM parkingLog";
	    $sql3 = "SELECT PIR3 FROM parkingLog";
	    $sql4 = "SELECT PIR4 FROM parkingLog";  
	    $result0 = $conn->query($sql0); 
	    $result1 = $conn->query($sql1);
	    $result2 = $conn->query($sql2);
	    $result3 = $conn->query($sql3);
	    $result4 = $conn->query($sql4);
	    $val0 = "NONE";
	    $val1 = 0;
	    $val2 = 0;
            $val3 = 0;
	    $val4 = 0;
	    $one = 1;
	    if ($result0->num_rows > 0) {
		$row = $result0->fetch_assoc();
	             echo "License Plate is: " . $row["LicensePlate"]. " <br>";
		     $val0 = $row["LicensePlate"];
	     } else {
	        echo "0 results";
	     }
	    if ($result1->num_rows > 0) {
		$row = $result1->fetch_assoc();
	             echo "Parking Space 1: " . $row["PIR1"]. " <br>";
		     $val1 = $row["PIR1"];
	     } else {
	        echo "0 results";
	     }
	     if ($result2->num_rows > 0) {
		$row = $result2->fetch_assoc();
	             echo "Parking Space 2: " . $row["PIR2"]. " <br>";
		     $val2 = $row["PIR2"];
	     } else {
	        echo "0 results";
	     }
	     if ($result3->num_rows > 0) {
		$row = $result3->fetch_assoc();
	             echo "Parking Space 3: " . $row["PIR3"]. " <br>";
		     $val3 = $row["PIR3"];
	     } else {
	        echo "0 results";
	     }
	     if ($result4->num_rows > 0) {
		$row = $result4->fetch_assoc();
	             echo "Parking Space 4: " . $row["PIR4"]. " <br>";
		     $val4 = $row["PIR4"];
	     } else {
	        echo "0 results";
	     }
     	     $conn->close();
     ?>

    <body>
	<newline> <br/> <newline>
	<div id="Introduction"> Here at the Parking Structure Monitor Company, we strive to give you the best parking experience. <br/>
	Below you will see four different parking spaces. The first space, an executive spot, is more  <br/>
	highly monitored as it gives the license plate number of a car parked there. The remaining three <br/>
	parking spots will show if a car is parked there or not. The green color indicates that the spot <br/>
	is clear and open. The red color indicates the spot is currently occupied. </div>
	<newline> <br/> <newline>
	<div id="wrapper">
		<div id="legend"> LEGEND </div>
		<div id="greensquare"> </div>
		<div id="gtitle"> &nbsp; Indicates an open spot </div>
		<newline> <br/> <newline>
		<newline> <br/> <newline>
		<div id="redsquare"> </div>
		<div id="rtitle"> &nbsp; Indicates an occupied spot </div>
	</div>
	<newline> <br/> <newline>
	<newline> <br/> <newline>
	<newline> <br/> <newline>
	<div id="PIR1_title"> Parking Space 1 *Executive Spot*</div>
	<div id="PIR1_title"> License Plate: <?php echo $val0; ?> </div>
	<div id="PIR1"> <?php if($val1 == $one){ ?> 1 <?php }else{ ?> 0 <?php } ?></div>
	<div id="PIR2_title"> Parking Space 2 </div>
	<div id="PIR2"> <?php if($val2 == $one){ ?> 1 <?php }else{ ?> 0 <?php } ?></div>
	<div id="PIR3_title"> Parking Space 3 </div>
	<div id="PIR3"> <?php if($val3 == $one){ ?> 1 <?php }else{ ?> 0 <?php } ?></div>
	<div id="PIR4_title"> Parking Space 4 </div>
	<div id="PIR4"> <?php if($val4 == $one){ ?> 1 <?php }else{ ?> 0 <?php } ?></div>
	
	
        <header>
            <nav id="main-navigation">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="page2.php">Live Video</a></li>
                </ul>
            </nav>
        </header>
        <div id="main-contents">
            Home Page
        </div>
        <footer>
            For more information for your Security Needs, you can reach us at ParkingSecurity@gmail.com
        </footer>
    </body>
</html>