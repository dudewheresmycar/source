import os
import time
import datetime
import glob
import MySQLdb
import socket
import RPi.GPIO as GPIO
from time import strftime


TCP_IP = '10.113.48.149' #Put your RPI Ip Address here
TCP_PORT = 5050
BUFFER_SIZE = 1024
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
data = s.recv(BUFFER_SIZE)

try:
	while True:
		# Variables for MySQL
		db = MySQLdb.connect(host="localhost", user="nbergam",passwd="pubg", db="Parking_database")
		cur = db.cursor()

		while True:
		    data = s.recv(BUFFER_SIZE)
		    int(data[0:4])
		    str(data[4:11])
		    PIR1 = data[0:1]
		    PIR2 = data[1:2]
		    PIR3 = data[2:3]
		    PIR4 = data[3:4]
		    LicensePlate = data[4:11]
		    print "\nPIR1 = " ,PIR1
		    print "\nPIR2 = " ,PIR2
		    print "\nPIR3 = " ,PIR3
		    print "\nPIR4 = " ,PIR4
		    print "\nLicence Plate is:  " ,LicensePlate
	            #time.sleep(5)
		    print "received data:", data
		    #PIR1 CODE
		    print PIR1
		    sql0 = ("""UPDATE parkingLog SET LicensePlate= ('%s')""" %(LicensePlate))
		    if PIR1 == "1":
		    	sql1 = ("""UPDATE parkingLog SET PIR1='1'""")
		    else:
			sql1 = ("""UPDATE parkingLog SET PIR1='0'""")
		
		    #PIR2 CODE
		    print PIR2
		    if PIR2 == "1":
		    	sql2 = ("""UPDATE parkingLog SET PIR2='1'""")
		    else:
			sql2 = ("""UPDATE parkingLog SET PIR2='0'""")
		
		    #PIR3 CODE
		    print PIR3
		    if PIR3 == "1":
		    	sql3 = ("""UPDATE parkingLog SET PIR3='1'""")
		    else:
			sql3 = ("""UPDATE parkingLog SET PIR3='0'""")
		
		    #PIR4 CODE
		    print PIR4
		    if PIR4 == "1":
		    	sql4 = ("""UPDATE parkingLog SET PIR4='1'""")
		    else:
			sql4 = ("""UPDATE parkingLog SET PIR4='0'""")
		
		    try:
		        print "Writing to database..."
		        # Execute the SQL command
			cur.execute(sql0)
		        cur.execute(sql1)
			cur.execute(sql2)
			cur.execute(sql3)
			cur.execute(sql4)
		        # Commit your changes in the database
		        db.commit()
		        print "Write Complete"
		 
		    except:
		        # Rollback in case there is any error
		        db.rollback()
		        print "Failed writing to database"
		 
		    cur.close()
		    db.close()
		    break

except KeyboardInterrupt: #If Control+C is pressed, exit and clean
	GPIO.cleanup() #clean up all the GPIO