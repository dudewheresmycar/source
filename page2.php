<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Parking Structure Video</title>
	<script type="text/javascript" src="jquery-3.2.1.js"></script>
	<script language="javascript" type="text/javascript">
	   $(document).ready(function(){
		   $('body').css('background-image', 'url("dust_scratches.png")');
		   $('#Header').css('font-size', '35px')
		   $('#Header').css('color', 'navy')
	});
	</script>
        <meta name="description" contents="Just a test website for learning html, css and php">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
	<div id="Header"> Live Video Stream </div>
	<div> Press this button to go to the Live Stream </div>
 	<a href="http://10.113.48.149:8080/stream_simple.html" 
	target="_blank">Live Stream</a>
	<div> Or Press Home to go back </div>
        <header>
            <nav id="main-navigation">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="page2.php">Live Video</a></li>
                </ul>
            </nav>
        </header>
        <div id="main-contents">
            Live Video of Parking Spaces
        </div>
        <footer>
            For more information for your Security Needs, you can reach us at ParkingSecurity@gmail.com
        </footer>
    </body>
</html>