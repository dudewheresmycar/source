# Project: Dude Wheres My Car #
* Group members: Alexander Han, Nick Bergamini, Cooper Gooch, Hughson Garnier

# Overview #

* Project Descriptions: This project is about parking sensor and public server management. In real life, private parking places can implement these systems in order to keep track of an available parking spot for customers. Moreover, it should record the time spent for current customers as well, possibly being used as a parking meter. It will also display what car has been there using machine vision (license plate identification). It will check if the license plate changes and if so, it will say it�s a different car. This entire project will be powered by a solar panel power system.


# Positions #
* Nick - Web Server, Database, & GUI.
* Alex - Propellor w/ Ping sensor to Detect Cars.
* Cooper - Machine Vision (Detecting License Plates).
* Sony - TCP Connection from Server & Client Pi. Power Delivery to all components. 


### For more details ###

* See Wiki page for how to get started, setup, and to see how the system is running or you could view the video [here](https://www.youtube.com/watch?v=s1hN8gjQ0go&feature=youtu.be)
